/**
 * @file	popcl.hpp
 * @author	CYRIL URBAN
 * @date	2017-10-15
 * @brief	The POP3 client.
 */

#include <string>
#include <iostream>
#include <sstream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <getopt.h>
#include <fstream>
#include <dirent.h>

#include "openssl/ssl.h"
#include "openssl/bio.h"
#include "openssl/err.h"

using namespace std;

// Function declaration:
void errEnd (string msg);
void okEnd ();
void openSocket ();
void sendData (string req);
void recvResponse (bool count);
string recvData ();
void readMessage (string num_of_msg);
void logIn ();
void getLogin ();
int main (int argc, char **argv);
void argsProcessing (int argc, char **argv);
void validArgs ();
void printHelp ();
void writeFile (string data, string num_of_msg);
void downloadAll ();
string toString (int num);
void openSsl();
void printMsg(string text);
void delAll();
string getID (string msg);
bool existID (string name);
