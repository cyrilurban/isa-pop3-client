all: popcl.cpp
	g++ -Wall -o popcl popcl.cpp -lssl -lcrypto

clean:
	rm -f popcl

# target for faster running in debugging
mail:
	rm -f maildir/*

run:
	./popcl pop3.seznam.cz -a cred -o maildir

ssl:
	./popcl pop3.seznam.cz -a cred -o maildir -T

scer:
	./popcl pop3.seznam.cz -a cred -o maildir -T -C /etc/ssl/certs

stls:
	./popcl pop3.seznam.cz -a cred -o maildir -S

del:
	./popcl pop3.seznam.cz -a cred -o maildir -T -d

new:
	./popcl pop3.seznam.cz -a cred -o maildir -T -n

ipv6:
	./popcl 2a02:598:a::78:46 -a cred -o maildir -T

bad:
	./popcl 2a02:598:a::78:46 -a cred -o maildir -d -n