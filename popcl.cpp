/**
 * @file	popcl.cpp
 * @author	CYRIL URBAN
 * @date	2017-10-15
 * @brief	The POP3 client.
 */

#include "popcl.hpp"

// Global variables:
// for networking
int sock;
SSL_CTX * c;
SSL * ssl;
// logIn
string username, password;
// arguments
bool t_encrypt, s_encrypt, del, new_msg, stls;
char *server = NULL;
char *port = NULL;
char *cer_file = NULL;
char *cer_dir = NULL;
char *auth_file = NULL;
char *out_dir = NULL;
// count of message
int msg_count;
int new_msg_count = 0;

/**
 * @brief      Main function
 *
 * @param[in]  argc  The argc
 * @param      argv  The argv
 *
 * @return     0 succes || 1 not succes 
 */
int main (int argc, char **argv) {	
	argsProcessing(argc, argv);
	validArgs();
	getLogin();

	// ssl
	if (t_encrypt) {
		openSocket();		
		openSsl();
		recvResponse(false);
	} else if (s_encrypt) {
		stls = false;  // flag for different type of send/recv
		openSocket();		
		
		sendData("STLS\r\n");
		recvResponse(false);		
		
		stls = true;
		openSsl();

	} else {	
		openSocket();		
	}

	logIn();

	if (del) {
		delAll();
		printMsg("Deleted");

	} else {
		downloadAll();
		printMsg("Downloaded");
	}

	okEnd();
}

/**
 * @brief      Opens a socket.
 */
void openSocket () {
	struct addrinfo ai;
	struct addrinfo *ai_rslt;
	struct addrinfo *ai_next;
	
	memset(&ai, 0, sizeof(struct addrinfo));	
	ai.ai_socktype = SOCK_STREAM;	
	ai.ai_protocol = 0;
	ai.ai_flags = 0;

	// try ipv4
	ai.ai_family = PF_INET; // ipv4
	if (getaddrinfo(server, port, &ai, &ai_rslt) != 0) {
		// try ipv6
		ai.ai_family = AF_INET6; // ipv6
		if (getaddrinfo(server, port, &ai, &ai_rslt) != 0) {		
			cerr << "Error: Connecting to server was failed.\n";
			exit(1);
		}
	}

	for (ai_next = ai_rslt; ai_next != NULL; ai_next = ai_next->ai_next) {
		sock = socket(ai_next->ai_family, ai_next->ai_socktype, ai_next->ai_protocol);
		
		if (sock < 0) {
			continue;
		}
		if (connect(sock, ai_next->ai_addr, ai_next->ai_addrlen) > -1) {
			break;
		}

		close(sock);
	}

	if (ai_next == NULL) {
		cerr << "Error: Connecting to server was failed.\n";
		exit(1);
	}

	freeaddrinfo(ai_rslt);
	
	if (!t_encrypt) {
		recvResponse(false);
	}
}

/**
 * @brief      Opens a ssl.
 */
void openSsl() {
	SSL_load_error_strings();
	OpenSSL_add_all_algorithms();
	SSL_library_init();

	// ssl context
	c = SSL_CTX_new(SSLv23_client_method());

	// load certificate
	int res;
	if (cer_dir == NULL && cer_file == NULL) {
		res = SSL_CTX_set_default_verify_paths(c);    
	} else {
		res = SSL_CTX_load_verify_locations(c, cer_file, cer_dir);
	}
	if (!res) {
		cerr << "Error: Loading trust store failed.\n";
		SSL_CTX_free(c);
		exit(1);
	}

	ssl = SSL_new(c);
	SSL_set_fd(ssl, sock);
	
	if (SSL_connect(ssl) != 1) {
		cerr << "SSL connect was failed" << endl;
		exit(1);
	}
	if (SSL_get_verify_result(ssl) != X509_V_OK) {
		cerr << "Error: Certificate verification was failed.\n";
		exit(1);
	}
}

/**
 * @brief      Returns a string representation of the int.
 *
 * @param[in]  num   The number
 *
 * @return     String representation of the int.
 */
string toString (int num) {
	ostringstream strng;
	strng << num;
	return strng.str();
}

/**
 * @brief      Downloads all.
 */
void downloadAll () {
	for (int i = 1; i < msg_count+1; i++) {
		readMessage(toString(i));
	}
}

/**
 * @brief      Deletes all.
 */
void delAll () {
	for (int i = 1; i < msg_count+1; i++) {
		sendData("DELE " + toString(i) + "\r\n");
		recvResponse(false);	
	}
}

/**
 * @brief      Print Help msg to stdout.
 */
void printHelp () {
	cout << "popcl.cpp [options]" << endl <<
	"-h | help msg" << endl <<
	"Compulsory:" << endl <<
	"-a | <auth_file>" << endl <<
	"-o | <out_dir>" << endl <<
	"     <server>" << endl <<
	"Optional:" << endl <<      
	"-p | <port>" << endl <<
	"-T | pop3s encrypt" << endl <<
	"-S | STLS encrypt" << endl <<
	"-c | <certfile>" << endl <<
	"-C | <certaddr>" << endl <<
	"-d | delete msg on server" << endl <<
	"-n | read only new msg" << endl;
}

/**
 * @brief      Arguments processing
 *
 * @param[in]  argc  The argc
 * @param      argv  The argv
 */
void argsProcessing (int argc, char **argv) { 
	server = NULL; // <server> - compulsory
	port = NULL; // -p <port>
	t_encrypt = false; // -T <pop3s>
	s_encrypt = false; // -S <STLS>
	cer_file = NULL; // -c <certfile>
	cer_dir = NULL; // -C <certaddr>	
	del = false; // -d <delete>
	new_msg = false; // -n <newmsg>
	auth_file = NULL; // -a <auth_file> - compulsory
	out_dir = NULL; // -o <out_dir> - compulsory

	int index;
	int c;
	opterr = 0;

	// init global variables
	while ((c = getopt(argc, argv, "p:TSc:C:dna:o:h")) != -1) {
		switch (c) {
			case 'p':
				port = optarg;
				break;
			case 'T':
				t_encrypt = true;
				break;
			case 'S':
				s_encrypt = true;
				break;
			case 'c':
				cer_file = optarg;
				break;
			case 'C':
				cer_dir = optarg;
				break;
			case 'd':
				del = true;
				break;
			case 'n':
				new_msg = true;
				break;
			case 'a':
				auth_file = optarg;
				break;
			case 'o':
				out_dir = optarg;
				break;
			case 'h':
				printHelp();
				exit(0);
				break;
			case '?':
				cerr << "Error: Wrong arguments. Use '-h' for help.\n";
				exit(1);
				break;
			default:
				break;
		}
	}

	// init server name (check duplicite)
	int i = 0;
	for (index = optind; index < argc; index++) {
		// more then 1 server name
		if (i > 0) {
			cerr << "Error: Wrong arguments. Use '-h' for help.\n";
			exit(1);
		}
		server = argv[index];
		i++;
	}
}

/**
 * @brief      Validation of argumentions.
 */
void validArgs () {
	// compulsory commands
	if (server == NULL || auth_file == NULL || out_dir == NULL) {
		cerr << "Error: Wrong arguments. Use '-h' for help.\n";
		exit(1);
	}

	// bad combination
	if (t_encrypt == false && s_encrypt == false && 
		(cer_file != NULL || cer_dir != NULL)) {
		cerr << "Error: Wrong arguments. Use '-h' for help.\n";
		exit(1);
	}

	// bad combination
	if (t_encrypt && s_encrypt) {
		cerr << "Error: Wrong arguments (bad combinations of -T and -S). Use '-h' for help.\n";
		exit(1);
	}

	// bad combination
	if (del && new_msg) {
		cerr << "Error: Wrong arguments (bad combinations of -n and -d). Use '-h' for help.\n";
		exit(1);
	}

	// default port number acording to encrypt
	if (port == NULL) {
		if (t_encrypt) {
			port = (char*)"995";
		} else {
			port = (char*)"110";
		}
	}
	
	// validation of out. dir
	if (DIR* dir = opendir(out_dir)) {
		closedir(dir);
	} else {
		cerr << "Error: Output folder does not exist.\n";
		exit(1);
	}

	// validation of auth. file
	if (FILE *file = fopen(auth_file, "r")) {
		fclose(file);
	} else {
		cerr << "Error: Authenticate file does not exist.\n";
		exit(1);
	}

	// validation of cert. file
	if (cer_file != NULL) {
		if (FILE *file = fopen(cer_file, "r")) {
			fclose(file);
		} else {
			cerr << "Error: Certificate file does not exist.\n";
			exit(1);
		}
	}
}

/**
 * @brief      Ending with error.
 * Sending err msg to stderr, send QUIT to server and
 * close connection.
 *
 * @param[in]  msg   The message
 */
void errEnd (string msg) {
	cerr << "Error: " << msg << "\n";	
	sendData("QUIT\r\n");

	// ssl encrypt
	if (t_encrypt || s_encrypt) {
		SSL_free(ssl); 
		SSL_CTX_free(c);
	}

	close(sock);
	exit(1);
}

/**
 * @brief      Ending with OK.
 * send QUIT to server and close connection.
 */
void okEnd () {
	sendData("QUIT\r\n");
	
	// ssl encrypt
	if (t_encrypt || s_encrypt) {
		SSL_free(ssl); 
		SSL_CTX_free(c);
	}

	close(sock);
	exit(0);
}

/**
 * @brief      Sends a data to server.
 *
 * @param[in]  req   The request
 */
void sendData (string req) {
	// ssl encrypt
	if (t_encrypt || stls) {
		if (SSL_write(ssl, req.c_str(), req.length()) < 0) {
			errEnd("Send data was failed.");
		}
	} else {
		if (write(sock, req.c_str(), req.length()) < 0) {
			errEnd("Send data was failed.");
		}
	}
}

/**
 * @brief      Receave response from server.
 * Determine if OK or Err.
 *
 * @param[in]  count  The count_flag
 */
void recvResponse (bool count) {
	char buffer[2];
	int bytesRead;
	string message;
	do {
		buffer[1] = buffer[0];
		
		// ssl encrypt
		if (t_encrypt || stls) {
			bytesRead = SSL_read(ssl, buffer, 1);
		} else {
			bytesRead = read(sock, buffer, 1);
		}

		if (bytesRead < 0) {
			errEnd("Unable to read from server.");
		} else {
			message += buffer[0];
		}				
	} while ((bytesRead != 0) && 
		!((buffer[1] == '\r') && (buffer[0] == '\n')));
	
	// receave +OK response
	if (!(message[0] == '+' && message[1] == 'O' && message[2] == 'K')) {
		// err cut "-ERR " for err messasge
		message.erase(0,5);
		errEnd(message);
	}

	if (count) {
		msg_count = atoi((message = message.substr(4, message.length())).c_str());
	}
}

/**
 * @brief      Print msg to stdout with result of operations.
 *
 * @param[in]  text  The text
 */
void printMsg(string text) {		
	if (new_msg) {
		if (new_msg_count <= 0) {
			cout << text + " 0 new message." << endl;
		} else if (new_msg_count == 1) {
			cout << text + " " + toString(new_msg_count) + " new message." << endl;
		} else {
			cout << text + " " + toString(new_msg_count) + " new  messages." << endl;
		}
	} else {
		if (msg_count <= 0) {
			cout << text + " 0 message." << endl;
		} else if (msg_count == 1) {
			cout << text + " " + toString(msg_count) + " message." << endl;
		} else {
			cout << text + " " + toString(msg_count) + " messages." << endl;
		}
	}
}

/**
 * @brief      Receave data from server.
 *
 * @return     string msg.
 */
string recvData () {   
	char buffer[5];
	int bytesRead;
	string message;
	do { 		
		buffer[4] = buffer[3];
		buffer[3] = buffer[2];
		buffer[2] = buffer[1];
		buffer[1] = buffer[0];
		
		// ssl encrypt
		if (t_encrypt || stls) {
			bytesRead = SSL_read(ssl, buffer, 1);
		} else {
			bytesRead = read(sock, buffer, 1);
		}

		if (bytesRead < 0) {
			errEnd("Unable to read from server.");
		} else {
			message += buffer[0];
		}
		
		if ((buffer[4] == '\r') && (buffer[3] == '\n') && 
			(buffer[2] == '.') && (buffer[1] == '\r') && 
			(buffer[0] == '\n')) {
			break;
		}		
	} while ((bytesRead != 0));
	
	return message;
}

/**
 * @brief      Writes a file.
 *
 * @param[in]  data        The data
 * @param[in]  num_of_msg  The number of message
 */
void writeFile (string data, string num_of_msg) {
	// get path
	string path;
	path += out_dir;
	path += "/";
	path += num_of_msg;

	// write file
	ofstream file;
	file.open(path.c_str());
	file << data;
	file.close();
}

/**
 * @brief      Gets the id from e-mail.
 *
 * @param[in]  msg   The message
 *
 * @return     The id.
 */
string getID (string msg) {
	// get id from header of e-mail
	istringstream iss(msg);
	string line;
	while (getline(iss, line)) {		
		if ((line.find("Message-Id: <") != string::npos) || (line.find("Message-ID: <") != string::npos)) {
			line.erase(0, 12);
			return line;
		}
	}
	// if id is not in header, create alternative id from msg. count of bytes 
	string alternativeID = "<";
	alternativeID += toString(msg.length());
	alternativeID += ">";
	return alternativeID;
}

/**
 * @brief      Check if exist ID.
 *
 * @param[in]  name  The name
 *
 * @return     true if exist | false otherwise
 */
bool existID (string name) {
	string path;
	path += out_dir;
	path += "/";
	path += name;

	if (FILE *file = fopen(path.c_str(), "r")) {
		fclose(file);
		return true;
	}
	return false;
}

/**
 * @brief      Reads a message.
 *
 * @param[in]  num_of_msg  The number of message
 */
void readMessage (string num_of_msg) {	
	sendData("RETR " + num_of_msg + "\r\n");		
	recvResponse(false);

	string buffer;
	buffer = recvData();
	// remove last 3 char (dot)
	buffer = buffer.substr(0, buffer.size()-3);

	string id = getID(buffer);
	
	if (new_msg) {
		if (!existID(id)) {
			writeFile(buffer, id);
			new_msg_count++;
		} 
	} else {
		writeFile(buffer, id);
	}
}

/**
 * @brief      Logs in.
 */
void logIn () {
	sendData("USER " + username + "\r\n");	
	recvResponse(false);

	sendData("PASS " + password + "\r\n");
	recvResponse(false);

	sendData("STAT\r\n");
	recvResponse(true);
}

/**
 * @brief      Gets the login from file.
 */
void getLogin () {
	string line;

	// open file
	ifstream myfile(auth_file);
	myfile.is_open();

	// read file line by line
	if (!(getline(myfile, line))) {
		cerr << "Error: Login file is incorrect.\n";
		exit(1);
	}
	if (line.length() < 12) {
		cerr << "Error: Login file is incorrect.\n";
		exit(1);
	}
	// get username	
	username = line.substr(11, line.length());
	if (!(getline(myfile, line))) {
		cerr << "Error: Login file is incorrect.\n";
		exit(1);
	}
	if (line.length() < 12) {
		cerr << "Error: Login file is incorrect.\n";
		exit(1);
	}
	// get password
	password = line.substr(11, line.length());	

	// close file
	myfile.close();
}
